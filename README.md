# Instalation

### Installing Gem
Add this line to application Gemfile
```
gem 'surveys313', path: 'path/to/surveys313'
```
and run
```
bundle install
```
### Migartions
```
rake surveys313:install:migrations
rake db:migrate
```
### config/routes.rb
```
mount Surveys313::Engine, at: "/name-of-the-surveys313-root-path"
```

### initializer
Inside application config/initializers folder create surveys313.rb file add line:
```
# class name of the survey answers relation model
Surveys313.user_class = "User"

# class name for metadata of the survey answer relation model
Surveys313.user_metadata_class_name = "UserMetadatum"

# foreign_key for the metdata
Surveys313.user_metadata_foreign_key = :user_id

# name of the has_many association of the relation model and metadata
Surveys313.user_metadata_relation = :user_metadata

# metadata data key name
Surveys313.user_metadata_key = :name

# metadata value name
Surveys313.user_metadata_value = :value

# name of the field for use identification
Surveys313.user_token_attribute = :unique_game_key

# relation name for the activity logs and relation model  
Surveys313.user_activity_logs_relation = :activity_logs

# enabling disabling logs
Surveys313.user_activity_logs_enabled = true

# helper method for the activity logs
# should accepts 2 params ([type of activity log], [note])   
Surveys313.user_activity_logs_helper_method = :create_user_activity_log

# type of the survey_logs 
Surveys313.user_activity_logs_survey_type = :survey_status_changed

# layout surveys  
Surveys313.layout = "surveys"
```

### Model
```
require 'surveys313/survey_extender'

class ModelClass < ActiveRecord::Base
   include SurveyExtender

   ...
end
```

This project rocks and uses MIT-LICENSE.