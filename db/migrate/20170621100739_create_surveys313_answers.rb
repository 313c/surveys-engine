class CreateSurveys313Answers < ActiveRecord::Migration
  def change
    create_table :surveys313_answers, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8', force: true do |t|
      t.text :answer
      t.integer :type
      t.integer :question_id
      t.timestamps null: false
    end
  end
end
