class CreateSurveys313Surveys < ActiveRecord::Migration
  def change
    
    create_table :surveys313_surveys, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8', force: true do |t|
      t.string :name
      t.string :slug, unique: true
      t.timestamps null: false
    end
  end
end
