class AddParentAnswerIdAndConditionRemoveDependsOn < ActiveRecord::Migration
  def change
    remove_column :surveys313_answers, :depends_on, :text
    add_column :surveys313_answers, :parent_answer_id, :integer
    add_column :surveys313_answers, :parent_answer_condition, :string
    add_column :surveys313_answers, :extra_information, :text
  end
end
