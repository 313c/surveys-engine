class CreateAnswerMetadata < ActiveRecord::Migration
  def change
    create_table :surveys313_answer_metadata, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8', force: true do |t|
      t.integer :answer_id
      t.string :key
      t.text :value
      t.timestamps null: false
    end
  end
end
