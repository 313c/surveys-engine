class CreateSurveys313SurveyMetadata < ActiveRecord::Migration
  def change
    create_table :surveys313_survey_metadata, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8', force: true do |t|
      t.string :key
      t.text :value
      t.integer :survey_id
      t.timestamps null: false
    end
    remove_column :surveys313_surveys, :description, :text
    remove_column :surveys313_surveys, :name, :string
  end
end
