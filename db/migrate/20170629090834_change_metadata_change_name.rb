class ChangeMetadataChangeName < ActiveRecord::Migration
  def change
    drop_table :surveys313_question_metadata if ActiveRecord::Base.connection.table_exists? 'surveys313_question_metadata'
    rename_table :surveys313_answer_metadata, :surveys313_question_metadata
  end
end
