class CreateSurveys313UserAnswers < ActiveRecord::Migration
  def change
    create_table :surveys313_user_answers, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8', force: true do |t|
      t.integer :user_id
      t.integer :answer_id
      t.integer :question_id
      t.integer :survey_id
      t.text :answer
      t.timestamps null: false
    end
  end
end
