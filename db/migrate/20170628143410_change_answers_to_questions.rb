class ChangeAnswersToQuestions < ActiveRecord::Migration
  def change
    create_table :surveys313_questions, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8', force: true do |t|
      t.integer :survey_id
      t.text :question
      t.integer :type
      t.integer :parent_question_id
      t.timestamps null: false
    end
    drop_table :surveys313_answers
  end
end
