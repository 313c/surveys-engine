class AddPolymorphicToUserAnswers < ActiveRecord::Migration
  def change
    add_column :surveys313_user_answers, :answerable_id, :integer
    add_column :surveys313_user_answers, :answerable_type, :string
    add_index :surveys313_user_answers, [:answerable_type,:answerable_id], name: :surveys313_user_answers_poly
  end
end
