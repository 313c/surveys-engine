class ChangeSurveyStatusFromNotSentToOpen < ActiveRecord::Migration
  def up
    umds = UserMetadatum.includes(:user).where("name LIKE 'survey_%_status' AND value LIKE 'not_sent%'")
    umds.each do |umd|
      s_id = umd.value.split("not_sent_").last
      umd.value = "open_#{s_id}"
      umd.save
      p "User ##{umd.user.id} #{umd.name} updated to #{umd.value}"
    end
  end
  def down
    umds = UserMetadatum.includes(:user).where("name LIKE 'survey_%_status' AND value LIKE 'open_%'")
    umds.each do |umd|
      s_id = umd.value.split("open_").last
      umd.value = "not_sent_#{s_id}"
      umd.save
      p "User ##{umd.user.id} #{umd.name} updated to #{umd.value}"
    end
  end
end
