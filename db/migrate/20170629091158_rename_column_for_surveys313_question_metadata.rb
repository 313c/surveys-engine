class RenameColumnForSurveys313QuestionMetadata < ActiveRecord::Migration
  def change
    rename_column :surveys313_question_metadata, :answer_id, :question_id
  end
end
