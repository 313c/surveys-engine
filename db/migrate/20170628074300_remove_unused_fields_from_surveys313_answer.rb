class RemoveUnusedFieldsFromSurveys313Answer < ActiveRecord::Migration
  def change
    remove_column :surveys313_answers, :type, :integer
    remove_column :surveys313_answers, :answer, :text
    remove_column :surveys313_answers, :options, :text
    remove_column :surveys313_answers, :parent_answer_condition, :string
    remove_column :surveys313_answers, :extra_information, :text
  end
end
  
