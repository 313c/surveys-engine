class CreateSurveys313Faqs < ActiveRecord::Migration
  def change
    create_table :surveys313_faqs, options: 'ENGINE=InnoDB DEFAULT CHARSET=utf8', force: true do |t|
      t.string :question
      t.text :answer
      t.integer :survey_id
      t.timestamps null: false
    end
  end
end
