Surveys313::Engine.routes.draw do
  scope ":slug", controller: :surveys, format: false, as: :survey do
    get :thank_you, path: 'potwierdzenie'
    get :already_finished, to: :thank_you, path: 'zakonczona', defaults: {already_finished: true}
    scope ":survey_token" do
      get :show, path: ''
      match :create, path: '', via: [:post, :put, :patch]
    end
  end
end
