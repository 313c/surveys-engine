# coding: utf-8
require 'json'
require 'config/recrutation_field' if Module.constants.include?(:RecrutationApp)
module Surveys313
  module Modules
    
    module RecruitmentsController
      def self.included(parent_class)
        parent_class.class_eval do
          
          if defined?( _edit_callback_callbacks)
            set_callback :edit_callback, :after do
              # surveys extension
          
              recruitment = self.instance_variable_get('@recruitment')
              @callback_surveys ||= Surveys313::Survey.active.for_type("Recruitment")
          
              finished_surveys = Surveys313::Survey.where(id: recruitment.finished_surveys)
              finished_surveys_hash = Hash.new
              finished_surveys.each do |survey|
                finished_surveys_hash[survey.id.to_s] = {survey: survey, questions: Array.new}
                survey.questions.each do |question|
                  finished_surveys_hash[survey.id.to_s][:questions] << {level: 0, question: question}
                  finished_surveys_hash[survey.id.to_s][:questions] << question.get_subquestions_for_level(1)
                end
                finished_surveys_hash[survey.id.to_s][:questions] = finished_surveys_hash[survey.id.to_s][:questions].flatten
              end
          
              self.instance_variable_set("@surveys", @callback_surveys)
              self.instance_variable_set("@finished_surveys_hash", finished_surveys_hash)
            end
          end
          
          # if defined?( _export_users_query_callback_callbacks)
          #   set_callback :export_users_query_callback, :before do
          #     @users = @users.includes(:survey_user_answers)
          #   end
          # end
          
          ####################################
          # Only if RecrutationApp is enabled
          ####################################
          
          if Module.constants.include?(:RecrutationApp)
            
            if defined?( _index_configurable_callback_callbacks)
              set_callback :index_configurable_callback, :after do
                                            
                @callback_surveys ||= Surveys313::Survey.active.for_type("Recruitment")
                
                #################
                # @list_columns #
                #################
                recrutation_config_columns = self.instance_variable_get('@recrutation_config_columns')
                
                user_status_index =  recrutation_config_columns.size-1
                @callback_surveys.each_with_index do |s,index|
                  insert_index = user_status_index + index + 1
                  survey_recrutation_field_config =  Surveys313::Modules::RecrutationApp::Config::RecrutationField.new({
                    key: "survey_#{s.id}_status",
                    type: :singleselect,
                    survey: s,
                    index: {
                      type: :editable,
                      include_blank: false,
                      additional_field: {
                          field: Surveys313::Modules::RecrutationApp::Config::RecrutationField.new({
                              key: "survey_#{s.id}_status_date",
                              type: :date
                            }),
                          key: "survey_#{s.id}_status_date",
                          editable: true
                        }
                      }
                    })
                    recrutation_config_columns.insert(insert_index,survey_recrutation_field_config)
                end
                self.instance_variable_set("@recrutation_config_columns",recrutation_config_columns)
                
                ########################
                # @user_config_filters #
                ########################
                recrutation_config_filters = self.instance_variable_get("@recrutation_config_filters")
                @callback_surveys.each do |s|
                  recrutation_config_filters << Surveys313::Modules::RecrutationApp::Config::RecrutationField.new({
                    key: "survey_#{s.id}_status".to_sym,
                    type: :singleselect,
                    survey: s,
                    filter: {type: :multiselect}
                  })
                end
                self.instance_variable_set("@recrutation_config_filters",recrutation_config_filters)
                
              end
              
            end
            
            # if defined?( _export_configurable_callback_callbacks)
            #   set_callback :export_configurable_callback, :after do
            #
            #     export_surveys ||= Surveys313::Survey.active.for_type("User").includes(:survey_metadata, questions: [:question_metadata, subquestions: [:question_metadata, subquestions: [:question_metadata, :subquestions]]])
            #
            #     export_surveys_hash = Hash.new
            #     export_surveys.each do |survey|
            #       export_surveys_hash[survey.id.to_s] = {survey: survey, questions: Array.new}
            #       survey.questions.each do |question|
            #         export_surveys_hash[survey.id.to_s][:questions] << {level: 0, question: question}
            #         export_surveys_hash[survey.id.to_s][:questions] << question.get_subquestions_for_level(1)
            #       end
            #       export_surveys_hash[survey.id.to_s][:questions] = export_surveys_hash[survey.id.to_s][:questions].flatten
            #     end
            #
            #     export_surveys_hash.each do |survey_id, survey|
            #       field_name = "survey_#{survey_id}_status".to_sym
            #       field_name_date = "#{field_name}_date".to_sym
            #
            #       key = "survey_#{survey_id}"
            #       headers = [
            #         Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_id".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.id'), survey: survey[:survey]}}),
            #         Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_name".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.name'), survey: survey[:survey]}}),
            #         Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_sent_date".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.sent_date',survey_name: survey[:survey].short_title), survey: survey[:survey]}}),
            #         Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_start_date".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.start_date',survey_name: survey[:survey].short_title), survey: survey[:survey]}}),
            #         Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_end_date".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.end_date',survey_name: survey[:survey].short_title), survey: survey[:survey]}})
            #       ]
            #
            #       survey[:questions].each_with_index do |question_hash, i|
            #         headers << Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_question_#{i}", export: {label: "#{survey[:survey].short_title} - #{question_hash[:question].label}", survey: survey[:survey], survey_question: question_hash[:question]}})
            #       end
            #
            #       @user_fields = @user_fields + headers
            #     end
            #   end
            # end
            #
            # if defined?( _individual_export_value_callback_callbacks)
            #   set_callback :individual_export_value_callback, :before do
            #     if @field.is_a?(::Surveys313::Modules::RecrutationApp::Config::UserField)
            #
            #       if @field.export.try(:survey_question).nil?
            #         case @field.key.to_s
            #         when "survey_#{@field.export.survey.id}_id"
            #           @value = @field.export.survey.id
            #         when "survey_#{@field.export.survey.id}_name"
            #           @value = @field.export.survey.name
            #         when "survey_#{@field.export.survey.id}_sent_date"
            #           @value = ldate(@value_source.survey_status_dates.try(:[],@field.export.survey.id.to_s).try(:[],'sent').try(:in_time_zone)) #Data wysłania
            #         when "survey_#{@field.export.survey.id}_start_date"
            #           @value = ldate(@value_source.survey_status_dates.try(:[],@field.export.survey.id.to_s).try(:[],'started').try(:in_time_zone)) #Data rozpoczęcia
            #         when "survey_#{@field.export.survey.id}_end_date"
            #           @value = ldate(@value_source.survey_status_dates.try(:[],@field.export.survey.id.to_s).try(:[],'finished').try(:in_time_zone)) #Data zakończenia
            #         end
            #       else
            #         if @field.export.survey_question.static_text?
            #           @value = '-'
            #         else
            #           answer = @value_source.get_full_survey_answer(@field.export.survey_question).try(:squish) || ""
            #           answer = numeric_user_answer(answer) if @field.export.survey_question.field_radio?
            #           @value = answer
            #         end
            #       end
            #     end
            #   end
            # end



          end
          
        end
      end
    end
    
    if Module.constants.include?(:RecrutationApp)
      module RecrutationApp
        module Config
          class RecrutationField < ::RecrutationApp::Config::RecrutationField
            def header *args
              self.survey.short_title.present? ? "Status #{self.survey.short_title}" : "Status #{self.survey.name}"
            end
            def index_header *args
              header args
            end
            def label *args
              header args
            end
            def get_dropdown_list *args
              Surveys313::Survey.statuses("survey_#{self.survey.id}_status").invert
            end
            def get_index_dropdown_list *args
              Surveys313::Survey.statuses("survey_#{self.survey.id}_status")
            end
          end
        end
      end
    end
  end
end
