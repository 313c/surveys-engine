module Surveys313
  module Modules
    module EmailTemplatesController
      # extend ActiveSupport::Concern
      def self.included(parent_class)
        parent_class.class_eval do
          
          
          # BUDIMEX SPECIFIC CALLBACKS
          if defined?(_shortcodes_list_callbacks)
            set_callback :shortcodes_list, :after do
              self.shortcodes_list_vars = {surveys: Surveys313::Survey.active}
              shortcodes = Array.new
              self.shortcodes_list_vars[:surveys].each do |survey|
                shortcodes << "<span><strong>[INDYWIDUALNY_LINK_DO_EWALUACJI #{survey.shortcode_slug}]</strong> - #{survey.shortcode_description}</span>"
              end
              self.shortcodes_list_vars = {shortcodes: shortcodes}
            end
          end
          
          if defined?(_send_email_callback_callbacks)
            set_callback :send_email_callback, :before do
              recipient = self.send_email_callback_vars[:recipient]
                            
              survey_slugs = ::EmailTemplate.survey_slugs self.send_email_callback_vars[:message]
              if survey_slugs.present?
                surveys = Surveys313::Survey.external_where(shortcode_slug: survey_slugs)
                unless surveys.empty?
                  surveys.each do |s|
                    recipient = (recipient.main_record || recipient) if recipient.is_a? ::User
                    recipient.update_survey_status! :sent, s, true
                    # main_user.activity_logs << create_user_activity_log(:survey_status_changed, I18n.t('surveys.activity_logs.statuses_changed', message: Surveys313::Survey.statuses["sent_#{s.id}"] ))
                  end
                end
              end
            end
          end
          
          # REST OF THE CALLBACKS
          if defined?(_shortcode_help_list_callbacks)
            set_callback :shortcode_help_list, :after do
              shortcodes = self.instance_variable_get('@shortcode_help_list')
              @surveys ||= Surveys313::Survey.for_type(current_source)
              shortcodes += @surveys.map{|s| "<span><strong>[INDYWIDUALNY_LINK_DO_EWALUACJI #{s.shortcode_slug}]</strong> - #{s.shortcode_description}</span>" }
              self.instance_variable_set('@shortcode_help_list', shortcodes)
            end
          end
          
        end
      end
    end
  end
end
