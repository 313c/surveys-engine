require 'json'
########################################
# Checking if the Vacancies module is enabled

if Module.constants.include?(:Vacancies)
  
  module Surveys313
    module Modules
      module GuardiansController
        def self.included(parent_class)
          parent_class.class_eval do
            if defined?(_edit_callback_callbacks)
              set_callback :edit_callback, :after do
                @can_read_surveys = can? :read_surveys, @guardian
                if @can_read_surveys
                  @surveys = Surveys313::Survey.active.for_type("Vacancies::Guardian")
                  @finished_surveys = Surveys313::Survey.where(id: @guardian.finished_surveys)
                  @finished_surveys_hash = Hash.new
                  @finished_surveys.each do |survey|
                    @finished_surveys_hash[survey.id.to_s] = {survey: survey, questions: Array.new}
                    survey.questions.each do |question|
                      @finished_surveys_hash[survey.id.to_s][:questions] << {level: 0, question: question}
                      @finished_surveys_hash[survey.id.to_s][:questions] << question.get_subquestions_for_level(1)
                    end
                    @finished_surveys_hash[survey.id.to_s][:questions] = @finished_surveys_hash[survey.id.to_s][:questions].flatten
                  end

                  # @activity_logs = @main_record.activity_logs
                  # @email_activity_logs = @main_record.email_activity_logs
                end
              end
            end
            if defined?(_index_columns_callbacks)
              set_callback :index_columns, :after do
                @callback_surveys ||= Surveys313::Survey.active.for_type("Vacancies::Guardian")
                hash_to_modify = self.index_columns_vars
                @callback_surveys.each do |s|
                  field_name = "survey_#{s.id}_status".to_sym
                  if hash_to_modify.try(:[],:row)
                    hash_to_modify[:row][field_name] = render_to_string(partial: '/surveys313/partials/survey_status', locals: {object: hash_to_modify[:object], can_update: true, survey_status_type: field_name, update_path: hash_to_modify[:update_path]})
                  end
                  if hash_to_modify.try(:[],:headers)
                    hash_to_modify[:headers].insert(-3, "Ewaluacja #{s.name}")
                  end
                  if hash_to_modify.try(:[],:js_table_columns_config)
                    arr = JSON.parse(hash_to_modify[:js_table_columns_config])
                    arr.insert(-4,{data: field_name,name: field_name})
                    hash_to_modify[:js_table_columns_config] = arr.to_json
                  end
                end
                self.index_columns_vars = hash_to_modify
              end
            end
            if defined?(_filters_callback_callbacks)
              set_callback :filters_callback, :after do
                hash_to_modify = self.filters_callback_vars
                
                # user for building filters on front
                if self.filters_callback_vars[:filters]
                  
                  @callback_surveys ||= Surveys313::Survey.active.for_type("Vacancies::Guardian")
                  @callback_surveys.each do |s|
                    hash_to_modify[:filters] << Surveys313::Modules::Vacancies::Config::GuardianTable.new({
                      key: "survey_#{s.id}_status".to_sym,
                      type: 'text',
                      filter_type: 'single',
                      survey: s,
                      field: Surveys313::Modules::Vacancies::Config::GuardianField.new({
                        key: "survey_#{s.id}_status".to_sym,
                        type: 'defined',
                        survey: s
                        })
                    })
                  end
                end
                            
                self.filters_callback_vars = hash_to_modify
              end
            end
            if defined?(_export_columns_callbacks)
              set_callback :export_columns, :after do
                hash_to_modify = self.export_columns_vars
                @export_surveys ||= Surveys313::Survey.active.for_type("Vacancies::Guardian").includes(:survey_metadata, questions: [:question_metadata, :subquestions])
                unless @export_surveys_hash.present?
                  @export_surveys_hash = Hash.new
                  @export_surveys.each do |survey|
                    @export_surveys_hash[survey.id.to_s] = {survey: survey, questions: Array.new}
                    survey.questions.each do |question|
                      @export_surveys_hash[survey.id.to_s][:questions] << {level: 0, question: question}
                      @export_surveys_hash[survey.id.to_s][:questions] << question.get_subquestions_for_level(1)
                    end
                    @export_surveys_hash[survey.id.to_s][:questions] = @export_surveys_hash[survey.id.to_s][:questions].flatten
                  end
                end
                
                @export_surveys.each do |s|
                  field_name = "survey_#{s.id}_status".to_sym
                  field_name_date = "#{field_name}_date".to_sym
                  
                  # additional headers for user export
                  if hash_to_modify.try(:[],:headers)
                    
                    headers = hash_to_modify[:headers].to_a
                    headers.insert(headers.size, [field_name, "#{s.name} status"], [field_name_date, "#{s.name} status (data)"])
                    hash_to_modify[:headers] = headers.to_h
                    
                    # survey questions
                    @export_surveys_hash.each do |survey_id, survey|
                      key = "survey_#{survey_id}"
                      hash_to_modify[:headers]["#{key}_id"] = I18n.t('surveys.headers.export.id')
                      hash_to_modify[:headers]["#{key}_name"] = I18n.t('surveys.headers.export.name')
                      hash_to_modify[:headers]["#{key}_sent_date"] = I18n.t('surveys.headers.export.sent_date',survey_name: survey[:survey].name)
                      hash_to_modify[:headers]["#{key}_start_date"] = I18n.t('surveys.headers.export.start_date',survey_name: survey[:survey].name)
                      hash_to_modify[:headers]["#{key}_end_date"] = I18n.t('surveys.headers.export.end_date',survey_name: survey[:survey].name)
                      survey[:questions].each_with_index do |question_hash, i|
                        hash_to_modify[:headers]["#{key}_question_#{i}"] = "#{survey[:survey].name} - #{question_hash[:question].label}"
                      end
                    end
                                    
                  end
                  
                  # additional row information for guardain export
                  if hash_to_modify.try(:[],:row)
                  
                    row = hash_to_modify[:row].to_a
                    guardian_survey_status = hash_to_modify[:guardian].try(field_name)
                    guardian_survey_status_date = hash_to_modify[:guardian].try(field_name_date).try(:in_time_zone).try(:to_date).try(:to_s)
                    row.insert(row.size,
                      [ field_name, Surveys313::Survey.statuses(guardian_survey_status)[guardian_survey_status]],
                      [ field_name_date, guardian_survey_status_date]
                    )
                    hash_to_modify[:row] = row.to_h
                    @export_surveys_hash.each do |survey_id, survey|
                      key = "survey_#{survey_id}"
                      hash_to_modify[:row]["#{key}_id"] = survey[:survey].try(:id)
                      hash_to_modify[:row]["#{key}_name"] = survey[:survey].try(:name)
                      hash_to_modify[:row]["#{key}_sent_date"] = ldate(hash_to_modify[:guardian].survey_status_dates.try(:[],survey_id.to_s).try(:[],'sent').try(:in_time_zone)) #Data wysłania
                      hash_to_modify[:row]["#{key}_start_date"] = ldate(hash_to_modify[:guardian].survey_status_dates.try(:[],survey_id.to_s).try(:[],'started').try(:in_time_zone)) #Data rozpoczęcia
                      hash_to_modify[:row]["#{key}_end_date"] = ldate(hash_to_modify[:guardian].survey_status_dates.try(:[],survey_id.to_s).try(:[],'finished').try(:in_time_zone)) #Data zakończenia
                      survey[:questions].each_with_index do |question_hash, i|
                        if question_hash[:question].static_text?
                          hash_to_modify[:row]["#{key}_question_#{i}"] = '-'
                        else
                          answer = hash_to_modify[:guardian].get_full_survey_answer(question_hash[:question]).try(:squish) || ""
                          answer = numeric_user_answer(answer) if question_hash[:question].field_radio?
                          hash_to_modify[:row]["#{key}_question_#{i}"] = answer
                        end
                      end
                    end
                    
                  end
                  
                end
                
                self.export_columns_vars = hash_to_modify
              end
            end
          end
        end
      end
      
      module Vacancies
        module Config
          class GuardianTable < ::Vacancies::Config::GuardianTable
            def header
              self.survey.name
            end
          end
          class GuardianField < ::Vacancies::Config::GuardianField
            def dropdown_values
              {brak: "Nie wysłano"}.merge(Surveys313::Survey.statuses("survey_#{self.survey.id}_status")).invert
            end
          end
        end
      end
      
    end
  end
  
end
