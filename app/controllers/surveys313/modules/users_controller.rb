# coding: utf-8
require 'json'
require 'config/user_field' if Module.constants.include?(:RecrutationApp)
module Surveys313
  module Modules
    
    module UsersController
      def self.included(parent_class)
        parent_class.class_eval do
          
          if defined?(_index_columns_callbacks)
            set_callback :index_columns, :after do
              @callback_surveys ||= Surveys313::Survey.active.for_type("User")
              hash_to_modify = self.index_columns_vars
              @callback_surveys.each do |s|
                field_name = "survey_#{s.id}_status".to_sym
                if hash_to_modify.try(:[],:row)
                  hash_to_modify[:row][field_name] = render_to_string(partial: '/surveys313/partials/survey_status', locals: {object: hash_to_modify[:object], can_update: true, survey_status_type: field_name, update_path: hash_to_modify[:update_path]})
                end
                if hash_to_modify.try(:[],:headers)
                  hash_to_modify[:headers][field_name] = s.name
                end
                if hash_to_modify.try(:[],:js_table_columns_config)
                  arr = JSON.parse(hash_to_modify[:js_table_columns_config])
                  arr << {data: field_name,name: field_name}
                  hash_to_modify[:js_table_columns_config] = arr.to_json
                end
              end
              self.index_columns_vars = hash_to_modify
            end
          end
          
          if defined?(_index_filters_callbacks)
            set_callback :index_filters, :after do
              @callback_surveys ||= Surveys313::Survey.active.for_type("User")
              hash_to_modify = self.index_filters_vars
              field_names = @callback_surveys.map{|s| "survey_#{s.id}_status".to_sym}
              
              # used for api backend callback
              if self.index_filters_vars[:columns_params].present?
                self.index_filters_vars[:columns_params].each do |id, column_data|
                  column_name = column_data['data'].try(:to_sym)
                  column_value = column_data['search']['value']
                  if field_names.include?(column_name) and column_value.present?
                    
                    # values = JSON.parse(column_value)
                    # query = ["name = ? and (",column_name.to_s]
                    # query[0] += values.map{ |v| "value like ?" }.join(' OR ');
                    # query += values.map{ |v| "%#{v}%"}
                    # query[0] += ")"
                    # hash_to_modify[:users] = hash_to_modify[:users].external_where(query)
                    
                    hash_to_modify[:users] = hash_to_modify[:users].external_where(column_name => (column_value == 'brak' ? nil : column_value))
                                      
                  end
                end
              end
              
              # user for building filters on front
              if self.index_filters_vars[:filters] or self.index_filters_vars[:from_to_filters]
                @callback_surveys.each do |s|
                  hash_to_modify[:filters][7][:items] << {
                    key: "survey_#{s.id}_status".to_sym,
                    name: "Status #{s.name}",
                    type: :singleselect,
                    options: {brak: "Nie wysłano"}.merge(Surveys313::Survey.statuses("survey_#{s.id}_status")).invert
                  }
                end
              end
              
              self.index_filters_vars = hash_to_modify
              
            end
          end
          
          if defined?(_export_columns_callbacks)
            set_callback :export_columns, :after do
              # @callback_surveys ||= Surveys313::Survey.active.for_type("User")
              hash_to_modify = self.export_columns_vars
              @export_surveys ||= Surveys313::Survey.active.for_type("User").includes(:survey_metadata, questions: [:question_metadata, :subquestions])
              unless @export_surveys_hash.present?
                @export_surveys_hash = Hash.new
                @export_surveys.each do |survey|
                  @export_surveys_hash[survey.id.to_s] = {survey: survey, questions: Array.new}
                  survey.questions.each do |question|
                    @export_surveys_hash[survey.id.to_s][:questions] << {level: 0, question: question}
                    @export_surveys_hash[survey.id.to_s][:questions] << question.get_subquestions_for_level(1)
                  end
                  @export_surveys_hash[survey.id.to_s][:questions] = @export_surveys_hash[survey.id.to_s][:questions].flatten
                end
              end
              
              @export_surveys.each do |s|
                field_name = "survey_#{s.id}_status".to_sym
                field_name_date = "#{field_name}_date".to_sym
                
                # additional headers for user export
                if hash_to_modify.try(:[],:headers)
                  
                  headers = hash_to_modify[:headers].to_a
                  before_index = headers.index{|h| h.first == :candidate_status_date}+1
                  headers.insert(before_index, [field_name, "#{s.name} status"], [field_name_date, "#{s.name} status (data)"])
                  before_index = headers.index{|h| h.first == :recruiter}+1
                  headers.insert(before_index,
                    [:real_practice_start, I18n.t('activerecord.attributes.user.real_practice_start')],
                    [:real_practice_end, I18n.t('activerecord.attributes.user.real_practice_end')],
                    [:practice_finished_prematurely, I18n.t('activerecord.attributes.user.practice_finished_prematurely')]
                  )
                  hash_to_modify[:headers] = headers.to_h
                  
                  # survey questions
                  @export_surveys_hash.each do |survey_id, survey|
                    key = "survey_#{survey_id}"
                    hash_to_modify[:headers]["#{key}_id"] = I18n.t('controllers.users.export.survey_headers.id')
                    hash_to_modify[:headers]["#{key}_name"] = I18n.t('controllers.users.export.survey_headers.name')
                    hash_to_modify[:headers]["#{key}_sent_date"] = I18n.t('controllers.users.export.survey_headers.sent_date',survey_name: survey[:survey].name)
                    hash_to_modify[:headers]["#{key}_start_date"] = I18n.t('controllers.users.export.survey_headers.start_date',survey_name: survey[:survey].name)
                    hash_to_modify[:headers]["#{key}_end_date"] = I18n.t('controllers.users.export.survey_headers.end_date',survey_name: survey[:survey].name)
                    survey[:questions].each_with_index do |question_hash, i|
                      hash_to_modify[:headers]["#{key}_question_#{i}"] = "#{survey[:survey].name} - #{question_hash[:question].label}"
                    end
                  end
                  
                end
                
                # additional row information for user export
                if hash_to_modify.try(:[],:row)
                  
                  row = hash_to_modify[:row].to_a
                  before_index = row.index{|r| r.first == :candidate_status_date}+1
                  user_survey_status = hash_to_modify[:main_record].try(field_name)
                  user_survey_status_date = hash_to_modify[:main_record].try(field_name_date).try(:in_time_zone).try(:to_date).try(:to_s)
                  row.insert(before_index,
                    [ field_name, Surveys313::Survey.statuses(user_survey_status)[user_survey_status]],
                    [ field_name_date, user_survey_status_date]
                  )
                  before_index = row.index{|h| h.first == :recruiter}+1
                  row.insert(before_index,
                    [:real_practice_start, hash_to_modify[:main_record].try(:real_practice_start)],
                    [:real_practice_end, hash_to_modify[:main_record].try(:real_practice_end)],
                    [:practice_finished_prematurely, I18n.t('models.resource.practice_finished_prematurely')[hash_to_modify[:main_record].practice_finished_prematurely.try(:to_sym)]]
                  )
                  hash_to_modify[:row] = row.to_h
                  
                  #survey questions
                  @export_surveys_hash.each do |survey_id, survey|
                    key = "survey_#{survey_id}"
                    hash_to_modify[:row]["#{key}_id"] = survey[:survey].try(:id)
                    hash_to_modify[:row]["#{key}_name"] = survey[:survey].try(:name)
                    hash_to_modify[:row]["#{key}_sent_date"] = ldate(hash_to_modify[:main_record].survey_status_dates.try(:[],survey_id.to_s).try(:[],'sent').try(:in_time_zone)) #Data wysłania
                    hash_to_modify[:row]["#{key}_start_date"] = ldate(hash_to_modify[:main_record].survey_status_dates.try(:[],survey_id.to_s).try(:[],'started').try(:in_time_zone)) #Data rozpoczęcia
                    hash_to_modify[:row]["#{key}_end_date"] = ldate(hash_to_modify[:main_record].survey_status_dates.try(:[],survey_id.to_s).try(:[],'finished').try(:in_time_zone)) #Data zakończenia
                    survey[:questions].each_with_index do |question_hash, i|
                      
                      
                      if question_hash[:question].static_text?
                        hash_to_modify[:row]["#{key}_question_#{i}"] = '-'
                      else
                        answer = hash_to_modify[:main_record].get_full_survey_answer(question_hash[:question]).try(:squish) || ""
                        answer = numeric_user_answer(answer) if question_hash[:question].field_radio?
                        hash_to_modify[:row]["#{key}_question_#{i}"] = answer
                      end
                    end
                  end
                  
                end
                
              end
              
              self.export_columns_vars = hash_to_modify
            end
          end
          
          if defined?( _edit_callback_callbacks)
            set_callback :edit_callback, :after do
              # surveys extension
              
              main_record = self.instance_variable_get('@main_record')
              @callback_surveys ||= Surveys313::Survey.active.for_type("User")
              
              finished_surveys = Surveys313::Survey.where(id: main_record.finished_surveys)
              finished_surveys_hash = Hash.new
              finished_surveys.each do |survey|
                finished_surveys_hash[survey.id.to_s] = {survey: survey, questions: Array.new}
                survey.questions.each do |question|
                  finished_surveys_hash[survey.id.to_s][:questions] << {level: 0, question: question}
                  finished_surveys_hash[survey.id.to_s][:questions] << question.get_subquestions_for_level(1)
                end
                finished_surveys_hash[survey.id.to_s][:questions] = finished_surveys_hash[survey.id.to_s][:questions].flatten
              end
              
              self.instance_variable_set("@surveys", @callback_surveys)
              self.instance_variable_set("@finished_surveys_hash", finished_surveys_hash)
            end
          end
          
          if defined?( _export_users_query_callback_callbacks)
            set_callback :export_users_query_callback, :before do
              @users = @users.includes(:survey_user_answers)
            end
          end
          
          ####################################
          # Only if RecrutationApp is enabled
          ####################################
          
          if Module.constants.include?(:RecrutationApp)
            
            if defined?( _index_configurable_callback_callbacks)
              set_callback :index_configurable_callback, :after do
                                            
                index_key = self.instance_variable_get("@index_key")
                
                if index_key.blank?
                  
                  @callback_surveys ||= Surveys313::Survey.active.for_type("User")
                  
                  #################
                  # @list_columns #
                  #################
                  list_columns = self.instance_variable_get('@list_columns')
                  
                  user_status_index =  list_columns.index{|uf| uf.key == :user_status}
                  @callback_surveys.each_with_index do |s,index|
                    insert_index = user_status_index + index + 1
                    survey_user_field_config =  Surveys313::Modules::RecrutationApp::Config::UserField.new({
                      key: "survey_#{s.id}_status",
                      type: :singleselect,
                      survey: s,
                      index: {
                        type: :editable,
                        include_blank: false,
                        additional_field: {
                            field: Surveys313::Modules::RecrutationApp::Config::UserField.new({
                                key: "survey_#{s.id}_status_date",
                                type: :date
                              }),
                            key: "survey_#{s.id}_status_date",
                            editable: true
                          }
                        }
                      })
                      list_columns.insert(insert_index,survey_user_field_config)
                  end
                  self.instance_variable_set("@list_columns",list_columns)
                  
                  ########################
                  # @user_config_filters #
                  ########################
                  user_config_filters = self.instance_variable_get("@user_config_filters")
                  @callback_surveys.each do |s|
                    user_config_filters << Surveys313::Modules::RecrutationApp::Config::UserField.new({
                      key: "survey_#{s.id}_status".to_sym,
                      type: :singleselect,
                      survey: s,
                      filter: {type: :multiselect}
                    })
                  end
                  self.instance_variable_set("@user_config_filters",user_config_filters)
                  
                end
                
              end
              
            end
            
            if defined?( _export_configurable_callback_callbacks)
              set_callback :export_configurable_callback, :after do
                if params['scope'].nil? or params['scope'].include? 'evaluations'
                  export_surveys ||= Surveys313::Survey.active.for_type("User").includes(:survey_metadata, questions: [:question_metadata, subquestions: [:question_metadata, subquestions: [:question_metadata, :subquestions]]])
              
                  export_surveys_hash = Hash.new
                  export_surveys.each do |survey|
                    export_surveys_hash[survey.id.to_s] = {survey: survey, questions: build_export_surveys_hash_questions(survey)}
                  end
              
                  export_surveys_hash.each do |survey_id, survey|
                    field_name = "survey_#{survey_id}_status".to_sym
                    field_name_date = "#{field_name}_date".to_sym
                    
                    key = "survey_#{survey_id}"
                    headers = [
                      Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_id".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.id'), survey: survey[:survey]}}),
                      Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_name".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.name'), survey: survey[:survey]}}),
                      Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_sent_date".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.sent_date',survey_name: survey[:survey].short_title), survey: survey[:survey]}}),
                      Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_start_date".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.start_date',survey_name: survey[:survey].short_title), survey: survey[:survey]}}),
                      Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_end_date".to_sym, export: {label: I18n.t('controllers.users.export.survey_headers.end_date',survey_name: survey[:survey].short_title), survey: survey[:survey]}})
                    ]
            
                    survey[:questions].each_with_index do |question_hash, i|
                      headers << Surveys313::Modules::RecrutationApp::Config::UserField.new({key: "#{key}_question_#{i}", export: {label: "#{survey[:survey].short_title} - #{question_hash[:question].label}", survey: survey[:survey], survey_question: question_hash[:question]}})
                    end
                    
                    @user_fields = @user_fields + headers
                  end
                end
              end
            end
            
            if defined?( _individual_export_value_callback_callbacks)
              set_callback :individual_export_value_callback, :before do
                if params['scope'].nil? or params['scope'].include? 'evaluations'
                  if @field.is_a?(::Surveys313::Modules::RecrutationApp::Config::UserField)
                    
                    if @field.export.try(:survey_question).nil?
                      case @field.key.to_s
                      when "survey_#{@field.export.survey.id}_id"
                        @value = @field.export.survey.id
                      when "survey_#{@field.export.survey.id}_name"
                        @value = @field.export.survey.name
                      when "survey_#{@field.export.survey.id}_sent_date"
                        @value = ldate((@value_source.try(:main_record) || @value_source).survey_status_dates.try(:[],@field.export.survey.id.to_s).try(:[],'sent').try(:in_time_zone)) #Data wysłania
                      when "survey_#{@field.export.survey.id}_start_date"
                        @value = ldate((@value_source.try(:main_record) || @value_source).survey_status_dates.try(:[],@field.export.survey.id.to_s).try(:[],'started').try(:in_time_zone)) #Data rozpoczęcia
                      when "survey_#{@field.export.survey.id}_end_date"
                        @value = ldate((@value_source.try(:main_record) || @value_source).survey_status_dates.try(:[],@field.export.survey.id.to_s).try(:[],'finished').try(:in_time_zone)) #Data zakończenia
                      end
                    else
                      if @field.export.survey_question.static_text?
                        @value = '-'
                      else
                        answer = @value_source.get_full_survey_answer(@field.export.survey_question).try(:squish) || ""
                        answer = numeric_user_answer(answer) if @field.export.survey_question.field_radio?
                        @value = answer
                      end
                    end
                  end
                end
              end
            end

          end
          
          def build_export_surveys_hash_questions survey, lvl=0, question=nil
            questions = Array.new
            (lvl==0 ? survey.questions : question.subquestions ).each do |question|
              unless question.related_competence_id.present? and !@competence_ids.include?(question.related_competence_id.to_i)
                questions << {level: lvl, question: question}
                questions << build_export_surveys_hash_questions(survey, lvl+1,question)
              end
            end
            questions.flatten
          end
          
        end
      end
    end
    
    if Module.constants.include?(:RecrutationApp)
      module RecrutationApp
        module Config
          class UserField < ::RecrutationApp::Config::UserField
            def header *args
              self.try(:export).try(:label) || (self.survey.short_title.present? ? "Status #{self.survey.short_title}" : "Status #{self.survey.name}")
            end
            def index_header *args
              header args
            end
            def label *args
              header args
            end
            def get_dropdown_list *args
              Surveys313::Survey.statuses("survey_#{self.survey.id}_status").invert
            end
            def get_index_dropdown_list *args
              Surveys313::Survey.statuses("survey_#{self.survey.id}_status")
            end
          end
        end
      end
    end
    
  end
end
