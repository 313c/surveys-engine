require_dependency "surveys313/application_controller"
module Surveys313
  class SurveysController < ApplicationController
    protect_from_forgery with: :exception
    
    def show
      @by_admin = (params[:by_admin] and send(Surveys313.devise_current_resource_method))
      survey_slug = params[:slug]
      @survey = Surveys313::Survey.find_by(slug: survey_slug)
      raise Errors::NotFound unless @survey
      object_class = @survey.type.constantize
      @obj = object_class.get_object_by_token(params[:survey_token], params[:slug])
      raise Errors::NotFound unless @obj
      
      # # Add related object to questions
      @survey.related_object = @obj
      if @obj.is_a? User and @obj.try(:recruitment).present?
        @related_competence_ids = @obj.recruitment.position.score_cart.competence_scores.ordered.survey_visible.select(:competence_id).map(&:competence_id)
      end
      
      # default_user = @user.default_record || @user
      # @description = @survey.get_description first_name: default_user.first_name, last_name: default_user.last_name
      # @description = @survey.get_description
      @description = @survey.description
                  
      unless @obj.survey_enabled?(@survey.id)
        redirect_to survey_already_finished_path @survey.slug
      else
        @obj.update_survey_status! :started, @survey
        # create_survey_user_activity_log @obj, I18n.t('surveys.activity_logs.statuses_changed', message: Surveys313::Survey.statuses["started_#{@survey.id}"])
      end
    end
    
    def create
      survey_slug = params[:slug]
      @survey = Survey.find_by(slug: survey_slug)
      
      object_class = @survey.type.constantize
      @obj = object_class.get_object_by_token params[:survey_token], params[:slug]
      
      raise Errors::NotFound unless @survey and @obj and @obj.survey_enabled?(@survey.id)
      unless @obj.survey_enabled?(@survey.id)
        redirect_to survey_already_finished_path @survey.slug
      else
        survey_questions = Surveys313::Question.where(survey_id: @survey.id)#.includes(subquestions: {subquestions: [:subquestions]})
        user_answers_params.each do |question_id, user_answer|
          @obj.survey_user_answers.build({question_id: question_id, survey_id: @survey.id, answer: user_answer}) if user_answer.present?
        end
        @obj.add_finished_survey @survey.id
        @obj.update_survey_status :finished, @survey
        @obj.save
        create_survey_user_activity_log @obj, I18n.t('surveys.activity_logs.statuses_changed', message: Surveys313::Survey.statuses["finished_#{@survey.id}"])
      end
      redirect_to survey_thank_you_path @survey.slug
    end
    
    def thank_you
      survey_slug = params[:slug]
      @survey = Survey.find_by(slug: survey_slug)
      @title = params[:already_finished] ? "Ankieta została już wypełniona!" : "Dziękujemy!"
      raise Errors::NotFound unless @survey
    end
    
    private
    
    def user_answers_params
      params[:survey_user_answers]
    end
    
  end
end
