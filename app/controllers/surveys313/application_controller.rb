module Surveys313
  class ApplicationController < ::ApplicationController
    protect_from_forgery with: :exception
    require 'errors/not_found'
    layout Rails.configuration.try(:surveys313).try(:[],:layout) || "surveys313/surveys"
    
    rescue_from Errors::NotFound, :with => :not_found
    def not_found
      raise ActionController::RoutingError.new('Not Found')
    end
    
    def create_survey_user_activity_log user, note
      # if Surveys313.user_activity_logs_enabled
      #   user.send(Surveys313.user_activity_logs_relation) << send(Surveys313.user_activity_logs_helper_method, Surveys313.user_activity_logs_survey_type, note)
      # end
    end
  end
end
