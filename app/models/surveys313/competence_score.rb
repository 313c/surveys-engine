module Surveys313
  module CompetenceScore
    def self.included(class_parent)
      class_parent.class_eval do
        external_attributes_for :competence_score_metadata, :key, :value, :survey_visible
        scope :survey_visible, ->{external_where(survey_visible: ["1","t","true"])}
      end
    end
  end
end
