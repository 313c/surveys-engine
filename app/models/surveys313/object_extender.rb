require "base64"
require "yaml"
module Surveys313
  module ObjectExtender
    def self.included(mod)
      
      mod::DEFAULTS[:class_parent].const_set(:SURVEYS313_CONFIG,{
        salt: "akdl3kd8fsl3k"
      }.merge(mod::DEFAULTS))
      
      mod::DEFAULTS[:class_parent].class_eval do
        
        external_attributes_for self::SURVEYS313_CONFIG[:metadata_association_name],
        self::SURVEYS313_CONFIG[:metadata_association_key],
        self::SURVEYS313_CONFIG[:metadata_association_value], *self::SURVEYS313_CONFIG[:surveys].map{|s| ["survey_#{s.id}_status".to_sym, "survey_#{s.id}_status_date".to_sym]}.flatten,
        :source,
        finished_surveys: {serialize: true},
        survey_status_dates: {serialize: true},
        surveys_filled_by_admin: {serialize: true}
        
        has_many :survey_user_answers, class_name: "Surveys313::UserAnswer", as: :answerable
        has_many :survey_archived_statuses, class_name: self::SURVEYS313_CONFIG[:metadata_association_class_name], foreign_key: self::SURVEYS313_CONFIG[:metadata_association_key]
        
        before_validation :update_survey_status_new_record, if: :new_record?
        
        # STATIC method to find user by survey_token
        def self.get_object_identifier token, survey_slug
          begin
            Base64.urlsafe_decode64(token).split(survey_slug).first
          rescue ArgumentError
            return false
          end
        end
        
        def get_survey_status_name survey_id
          key = self.send("survey_#{survey_id}_status")
          Surveys313::Survey.statuses(key)[key]
        end
        
        def get_survey_answer question_id
          self.survey_user_answers.detect{|ua| ua.question_id == question_id}
        end
        
        def get_full_survey_answer question
          ua = get_survey_answer(question.id)
          ua.present? ? question.get_user_answer(ua.answer) : nil
        end
        
        # get all the surveys which user already finished
        def get_finished_surveys
          self.finished_surveys || []
        end
        
        # mark survey as finished by user
        def add_finished_survey survey_id
          self.finished_surveys = (self.get_finished_surveys.clone << survey_id).uniq
          self.get_finished_surveys
        end
        
        # remove survey from the :finished_surveys list
        def remove_finished_survey survey_id
          arr = self.get_finished_surveys
          arr.delete(survey_id)
          self.finished_surveys = arr
          self.get_finished_surveys
        end
        
        
        # get the individual user survey token
        def get_survey_token survey_slug
          Base64.urlsafe_encode64("#{self.send(self.class::SURVEYS313_CONFIG[:identifier])}#{survey_slug}#{self.class::SURVEYS313_CONFIG[:salt]}")
        end
        
        # check if survey is enabled for user
        def survey_enabled? survey_id
          !get_finished_surveys.include?(survey_id)
        end
        
        def update_survey_status_new_record skip_if_present=false
          Surveys313::Survey.active.for_type(self.class.name).each do |s|
            update_survey_status :open, s, skip_if_present
          end
        end
        
        # update survey_status and archive actual
        def update_survey_status status_name, survey, skip_if_present=false
          status_type = "survey_#{survey.id}_status"
          return false if skip_if_present and self.send(status_type).present? and self.send(status_type) != "open_#{survey.id}"
          case status_name.to_sym
          when :open
            self.send("#{status_type}=", "open_#{survey.id}")
          when :sent
            self.send("#{status_type}=", "sent_#{survey.id}")
          when :started
            self.send("#{status_type}=", "started_#{survey.id}")
          when :finished
            self.send("#{status_type}=", "finished_#{survey.id}")
          end
          set_survey_status_date survey
          true
        end
        
        def update_survey_status! status_name, survey, skip_if_present=false
          self.update_survey_status(status_name, survey, skip_if_present) ? self.save : false
        end
        
        def update_survey_status_date! survey, time
          set_survey_status_date survey, time, true
          self.save
        end
        
        private
        
        def set_survey_status_date survey, time=nil, force=false
          if force or (self.send("survey_#{survey.id}_status_changed?") and self.send("survey_#{survey.id}_status") != "open_#{survey.id}")
            time ||= Time.current.utc
            survey_id = self.send("survey_#{survey.id}_status").split("_").last
            status = self.send("survey_#{survey.id}_status").split("_").first
            self.send("survey_#{survey.id}_status_date=",time)
            
            ssd = self.survey_status_dates.try(:dup) || Hash.new
            ssd[survey_id] = (ssd[survey_id] || Hash.new).merge({status => time})
            self.survey_status_dates = ssd
          end
        end
        
      end
      
    end
  end
end
