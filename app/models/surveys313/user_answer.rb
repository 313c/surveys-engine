module Surveys313
  class UserAnswer < ActiveRecord::Base
    # isolate_namespace Surveys313
    belongs_to :answerable, polymorphic: true
    belongs_to :question
  end
end
