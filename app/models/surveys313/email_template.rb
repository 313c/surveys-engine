module Surveys313
  module EmailTemplate
    extend ActiveSupport::Concern
    included do
      
      class << self
        def survey_slugs message
          message.scan(/\[INDYWIDUALNY_LINK_DO_EWALUACJI (.+)\]/).try(:flatten)
        end
      end
      
      after_parse_shortcodes do
        if self.shortcodes_main_user
          self.shortcodes_content = self.shortcodes_content.gsub /\[INDYWIDUALNY_LINK_DO_EWALUACJI (.+)\]/ do |shortcode|
            slug = shortcode.split(" ").last.gsub(']','');
            survey = Surveys313::Survey.external_where(shortcode_slug: slug).first
            url = "#{Rails.configuration.try(:client_domain) || "http://#{Rails.configuration.try(:front_domain)}"}/kwestionariusz/#{survey.slug}/#{self.shortcodes_main_user.get_survey_token survey.slug}"
            survey.blank? ? survey : "<a href='#{url}'>#{url}</a>"
          end
        end
      end
    end
  end
end
