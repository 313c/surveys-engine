module Surveys313
  class QuestionMetadatum < ActiveRecord::Base
    belongs_to :question, class_name: "Surveys313::Question", foreign_key: :question_id
  end
end
