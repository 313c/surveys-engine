module Surveys313
  module User
    def self.included(class_parent)
      self.const_set(:DEFAULTS,{
        class_parent: class_parent,
        metadata_association_name: :user_metadata,
        metadata_association_class_name: "UserMetadatum",
        metadata_association_key: :name,
        metadata_association_value: :value,
        identifier: :unique_game_key,
        surveys: Surveys313::Survey.active.for_type(class_parent.name)
      })
      include Surveys313::ObjectExtender
      
      class_parent.class_eval do
        before_validation :set_source_to_formularz, if: :new_record?
        before_validation :copy_survey_status_from_main_record, if: :duplicate_of_changed?
        
        class << self
          def get_object_by_token token, slug
            identifier = self.get_object_identifier token, slug
            DEFAULTS[:class_parent].external_where(DEFAULTS[:identifier] => identifier).first
          end
        end
        def set_source_to_formularz
          self.source = :formularz
        end
        
        alias_method :main_update_survey_status_new_record, :update_survey_status_new_record
        def update_survey_status_new_record
          self.main_update_survey_status_new_record unless self.try(:is_super_user)
        end
        
        def copy_survey_status_from_main_record
          if self.main_record.present?
            Surveys313::Survey.active.for_type("User").each do |s|
              self.send("survey_#{s.id}_status=",self.main_record.send("survey_#{s.id}_status"))
              self.send("survey_#{s.id}_status_date=",self.main_record.send("survey_#{s.id}_status_date"))
            end
          end
        end
        
        
        if defined?( _add_duplicate_metadata_callbacks )
          set_callback :add_duplicate_metadata, :after do
            surveys = Surveys313::Survey.active.for_type("User").select(:id)
            hash_to_modify = self.add_duplicate_metadata_vars
            hash_to_modify[:attributes] = hash_to_modify[:attributes] + [
                :practice_begin_date,
                :practice_end_date,
                :program_of_practice,
                :time_of_practice,
                :practice_protector,
                :practice_decision,
                :real_practice_start,
                :real_practice_end,
                :practice_finished_prematurely,
                :vacancy_id,
                :source,
                :referring_guardian_id
              ] + surveys.map{|s| ["survey_#{s.id}_status".to_sym,"survey_#{s.id}_status_date"]}.flatten
            self.add_duplicate_metadata_vars = hash_to_modify
          end
        end
      
        if defined?( _duplicate_user_fields_callbacks )
          set_callback :duplicate_user_fields, :after do
            survey_ids = Surveys313::Survey.active.for_type("User").select(:id)
            duplicate_keys = [:source] + survey_ids.map{|s| ["survey_#{s.id}_status".to_sym,"survey_#{s.id}_status_date"]}.flatten
            @duplicate_user_field_keys = (@duplicate_user_field_keys || []) + duplicate_keys
          end
        end
        
      end
                  
    end
  end
end
