require_dependency 'external_attributes'
require_dependency 'surveys313/shortcodes'
module Surveys313
  class Question < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
    attr_accessor :related_object
    
    extend ExternalAttributes
    external_attributes_for :question_metadata,
    :key,
    :value,
    :question,
    :include_blank,
    :layout,
    :placeholder,
    :related_competence_id,
    multiple: {serialize: true},
    validation: {serialize: true},
    enables: {serialize: true},
    parent_question_condition: {serialize: true},
    dropdown_options: {serialize: true},
    dropdown_options_depends_on: {serialize: true},
    dropdown_with_groups: {serialize: true},
    subquestions_for_options: {serialize: true}
        
    enum type: [:static_text,:field_single, :field_radio,:field_multi,:field_open,:field_text,:field_dropdown,:field_dropdown_other,:field_date]
    
    has_many :user_answers, class_name: "Surveys313::UserAnswer"
    has_many :subquestions, class_name: "Surveys313::Question", foreign_key: :parent_question_id, dependent: :destroy, autosave: true do
      def with_related_object(obj)
        each {|q| q.related_object = obj}
      end
    end
    belongs_to :parent_question, class_name: "Surveys313::Question", foreign_key: :parent_question_id
    has_many :question_metadata, class_name: "Surveys313::QuestionMetadatum", foreign_key: :question_id, autosave: true, dependent: :destroy
    
    alias_method :before_shortcode_question, :question
    def question
      Surveys313::Shortcodes.parse(before_shortcode_question, self.related_object)
    end
    
    
    def get_dropdown_options
      if dropdown_options_depends_on.blank?
        dropdown_options.invert
      else
        ret_options = []
        dropdown_options.each do |source_id, conditions|
          conditions.each do |condition, options|
            options.each do |value, name|
              ret_options << [name, value, {data: {source_id: source_id, condition: condition}}]
            end
          end
        end
        ret_options
      end
    end
    
    def validation_data
      return "" unless validation.present?
      return_hash = {validation: ""}
      validation.each do |name, condition|
        return_hash[:validation] += " required" if name == :require and condition
      end

      if field_date?
        return_hash[:validation] += ' date'
        return_hash['validation-format'] = 'yyyy-mm-dd'
      end

      return_hash
    end
    
    def is_required?
      validation.try(:keys).try(:include?, :require) ? true : false
    end
    
    def get_subquestions_for_level lvl
      return [] if subquestions.empty?
      ret_arr = Array.new
      subquestions.each do |q|
        ret_arr << {level: lvl, question: q}
        ret_arr << q.get_subquestions_for_level(lvl+1)
      end
      ret_arr
    end
    
    def data value=nil
      if value.nil?
        enables_data = self.enables.present? ? {enables: self.enables} : {}
      else
        enables_data = self.enables.present? ? {enables: self.enables.select{|k,v| v.include? value or v.blank?}.inject({}){|memo,(k,v)| memo[k] = [value];memo}} : {}
      end
      dropdown_options_depends_on_data = self.dropdown_options_depends_on.blank? ? {} : {dropdown_options_depends_on: self.dropdown_options_depends_on}
      validation_data = self.validation.present? ? self.validation_data : {}
      [enables_data, validation_data, dropdown_options_depends_on_data].inject(&:merge)
    end
    
    def get_user_answer answer
      #OPEN , , , question.field_dropdown? ||| question.dropdown_options[ua.try(:answer)]
      if answer.blank?
        return 'b.d.'
      elsif self.field_open? or self.field_date? or self.field_text?
        return answer
      elsif self.field_dropdown? or self.field_radio? or self.field_dropdown_other?
        if self.dropdown_options_depends_on
          return self.dropdown_options.values.map{|opts| opts.values}.flatten.reduce(Hash.new, :merge)[answer]
        elsif self.multiple
          dropdown_hash = self.dropdown_options.values.reduce(Hash.new, :merge).invert
          return JSON.parse(answer).map{|loc| dropdown_hash[loc.to_sym] }.join(", ")
        else
          return self.dropdown_options[answer]
        end
      end
    end
    
    def label
      self.question || self.placeholder || self.try(:parent_question).try(:question) || false
    end
    
    ####################
    # layout functions #
    ####################
    def is_layout_field_radio_subquestions?() self.layout == "field_radio_subquestions" end
    def is_layout_field_inline?() self.layout == "field_inline" end
    
  end
end
