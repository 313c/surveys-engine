module Surveys313
  class Faq < ActiveRecord::Base
    belongs_to :survey, class_name: Surveys313::Survey, foreign_key: :survey_id
  end
end
