module Surveys313
  module Recruitment
    def self.included(class_parent)
      self.const_set(:DEFAULTS,{
        class_parent: class_parent,
        metadata_association_name: :recruitment_metadata,
        metadata_association_class_name: "RecruitmentMetadatum",
        metadata_association_key: :key,
        metadata_association_value: :value,
        identifier: :id,
        surveys: Surveys313::Survey.active.for_type(class_parent.name)
      })
      include Surveys313::ObjectExtender
      
      class_parent.class_eval do
                
        class << self
          def get_object_by_token token, slug
            identifier = self.get_object_identifier token, slug
            DEFAULTS[:class_parent].find_by(DEFAULTS[:identifier] => identifier)
          end
        end
        
      end
                  
    end
  end
end
