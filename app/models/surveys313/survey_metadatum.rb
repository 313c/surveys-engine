module Surveys313
  class SurveyMetadatum < ActiveRecord::Base
    belongs_to :survey, class_name: 'Surveys313::Survey'
  end
end
