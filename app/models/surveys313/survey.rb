require_dependency 'external_attributes'
require_dependency 'surveys313/shortcodes'
module Surveys313
  class Survey < ActiveRecord::Base
    attr_accessor :related_object
    extend ExternalAttributes
    external_attributes_for :survey_metadata,
      :key,
      :value,
      :description,
      :thank_you_page,
      :thank_you_btn_href,
      :name,
      :title,
      :short_title,
      :type,
      :shortcode_slug,
      :shortcode_description
    
    enum status: {
      archived: 0,
      active: 1
    }
    
    before_destroy :destroy_all_related_object_statuses
    
    has_many :questions, ->{order(:created_at, :id)}, dependent: :destroy do
      def with_related_object(obj)
        each{|q| q.related_object = obj}
      end
    end
    has_many :survey_metadata, class_name: "Surveys313::SurveyMetadatum", foreign_key: :survey_id, autosave: true, dependent: :destroy
    has_many :faq_questions, class_name: Surveys313::Faq, foreign_key: :survey_id, dependent: :destroy

    default_scope { where(version: Rails.application.config.version) } unless Rails.application.config.try(:version).nil?
    scope :available, ->{where("1=1")}
    scope :get_user_surveys, ->{where('1=1')}
    scope :for_type, ->(type){external_where(type: type)}
    
    
    unless Rails.application.config.try(:version).nil?
      amoeba do
        set version: '2018'
        enable
      end
    end

    def destroy_all_related_object_statuses
      if self.type.constantize.is_a? User
        ::User.main_records.each do |u|
          u.send("survey_#{self.id}_status=",nil)
          u.send("survey_#{self.id}_status_date=",nil)
          u.save
        end
      elsif self.type.constantize.is_a? Recruitment
        ::Recruitment.main_records.each do |r|
          r.send("survey_#{self.id}_status=",nil)
          r.send("survey_#{self.id}_status_date=",nil)
          r.save
        end
      end
    end

    # variables[:first_name]
    # variables[:last_name]
    def get_description variables
      @get_description = description unless defined?(@get_description)
      @get_description = @get_description.gsub '[IMIE]', variables[:first_name] if variables[:first_name]
      @get_description = @get_description.gsub '[NAZWISKO]', variables[:last_name] if variables[:last_name]
      return @get_description
    end
    
    def self.statuses survey_status_type=nil
      survey_id = survey_status_type.to_s.split("_")[1]
      {
        "open_#{survey_id}" => I18n.t("surveys.statuses.open"),
        "sent_#{survey_id}" => I18n.t("surveys.statuses.sent"),
        "started_#{survey_id}" => I18n.t("surveys.statuses.started"),
        "finished_#{survey_id}" => I18n.t("surveys.statuses.finished"),
      }
    end
    alias_method :before_shortcode_description, :description
    def description
      Surveys313::Shortcodes.parse(before_shortcode_description, self.related_object)
    end
        
  end
end
