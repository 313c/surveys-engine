$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "surveys313/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "surveys313"
  s.version     = Surveys313::VERSION
  s.authors     = ["Rafal"]
  s.email       = ["rafal@codephonic.com"]
  s.homepage    = "http://codephonic.com"
  s.summary     = "The surveys extension for 313 projects"
  s.description = "The surveys extension for 313 projects"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.7.1"
  s.add_development_dependency "mysql2"
  s.add_development_dependency "rspec"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "factory_girl_rails"
  s.add_dependency "amoeba"
    
end
