module Surveys313
  class Engine < ::Rails::Engine
    isolate_namespace Surveys313
    
    initializer "surveys313.assets.precompile" do |app|
      app.config.assets.precompile += %w( surveys313/surveys.js surveys313/surveys.css )
    end
  end
end
