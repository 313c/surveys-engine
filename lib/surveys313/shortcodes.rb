module Surveys313
  class Shortcodes
    class << self
      def parse message, obj
        if obj.present?
          if obj.is_a? User
            message = message.gsub '[NAZWA_DEALERA]', "#{obj.try(:recruitment).try(:client)} #{obj.try(:recruitment).try(:localization).try(:name)}"
            message = message.gsub '[IMIE]', "#{obj.first_name}"
            message = message.gsub '[NAZWISKO]', "#{obj.last_name}"
          elsif obj.is_a? Recruitment
            message = message.gsub '[NAZWA_DEALERA]', "#{obj.try(:client)} #{obj.try(:localization).try(:name)}"
            message = message.gsub '[STANOWISKO]', "#{obj.try(:position).try(:name)}"
          end
        end
        message
      end
    end
  end
end
