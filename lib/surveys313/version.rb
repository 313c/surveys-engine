module Surveys313
  VERSION = "2.1.14"
  # v2.1.14
  # - fix issue with missing status dates inside export file for duplicate records
  # v2.1.13
  # - save after update_survey_status_date
  # v2.1.12
  # - add object_extender method to update_survey_status_date!
  # v2.1.11
  # - chanign conditions to require RecrutationApp modules
  # v2.1.10
  # - fix issue with missing status for duplicate records
  # v2.1.9
  # - fix issue with backward compatibility (budimex)
  # v2.1.8
  # - backward compatibility user front_domain if client_domain doesn't exists
  # v2.1.7
  # - fix issue with wrong result titles
  # v2.1.6
  # - backward compatibility (Guardians)
  # v2.1.5
  # - backward compatibility (BX)
  # v2.1.4
  # - add CompetenceScore module to handle survey question competences visibility conditions
  # v2.1.3
  # - setup evaluation export with new competence structure
  # v2.1.2
  # - get survey question competences by new copetences relation structure
  # v2.1.1
  # - change _survey_evaluation.html.erb without form tag
  # v2.1.0
  # - add survey functionality for Recruitments
  # v2.0.34
  # - add shortcodes
  # - add related_competence_id to Surveys313::Question class
  # - destroy all object statuse when survey destroyed
  # v2.0.33
  # - modify email tempalte helpers to load suerveys only for specific type of the survey
  # v2.0.32
  # - small fix for survey sent status
  # v2.0.31
  # - fix duplicate records and add callback for duplicate records
  # v2.0.30
  # - change the slug of the status from not_sent to open
  # v2.0.29
  # - duplicate fix from version v2.0.26
  # v2.0.28
  # - change filters from singleselct to multiselct
  # v2.0.27
  # - new "not_sent" survey status which is set automatically after user is created
  # - modify filters and index dropdown values
  # v2.0.26
  # - fix export and change title of survey export fields
  # v2.0.25
  # - change survey evaluation tags fro name to  short title
  # v2.0.24
  # - change require to require_dependency for loading surveys313/shortcodes
  # v2.0.23
  # - update front [NAZWA_DEALERA] shortcode to display client and localization name instead of same localization
  # v2.0.22
  # - add shortcode slug and description to surveys
  # - modify functionality to user shortcode_slug and shortcode_description
  # v2.0.21
  # - fix surveys link
  # v2.0.20
  # - change the host of the survey generated link to use config client_domain instead of the front_domain.
  # v.2.0.19
  # - change order of object list column for statuses,
  # - add short_title for survey
  # v2.0.17
  # - modify shortcodes to disable error when question is not related with any object
  # - update survey status print inside edit tab view
  # v2.0.16
  # - add shortcodes for question content
  # V2.0.15
  # - fix issue with add_finished_surveys for multiple surveys per object
  # v2.0.14
  # - add shortcodes to display question and description of the survey
  # v2.0.13
  # - add export configurable callbacks
  # v2.0.12
  # - modify configurable callback to modify filters
  # v2.0.11
  # - add require 'config/user_field' to users_controller.rb module
  # v2.0.10
  # - checking if the Vacancies module is enabled before defining GuardiansController module
  # v2.0.9
  # - add new edit_callback
  # - add new index_configurable_callback
  # - checking if versioning is enabled inside survey.rb
  # - changing #id of the survey_evaluation.html.erb
  # - changing to user front_domain or current_domain - depends of the rails configuration
  # - add timestamps for survey questions table
  # v2.0.8
  # - change defining custom layout for surveys front by rails configuration file
  # - checkng if versioning is enabled if so then default scope by newest version
  # - changing layout of the front survey show (THIS WILL CHANE LAYOUT OF THE OTHER APPLICATIONS, NEED TO BE FIXED INSIDE CUSTOM VIEW LAYOUT)
  # v2.0.7
  # - add callback for set status when sendin email with survey shortcodes
  # v2.0.6
  # - add survey_(id)_statu_date for user set_duplicates_meta
  # v2.0.5
  # - add conditions to check if each of the callback was set inside parent class
  # v2.0.4
  # - modify migrations to drop tables if exists
  # v2.0.3
  # - fix dup method for nil
  # v2.0.2
  # - fix saving survey_status_dates
  # - adding export callback functionality
  # v2.0.1
  # - add callback for admin users export
end
