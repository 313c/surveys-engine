require "surveys313/engine"
module Surveys313
  mattr_accessor :user_class, instance_accessor: false do
    "User"
  end
  def self.user_class
    @@user_class.constantize
  end
  mattr_accessor :user_metadata_class_name, instance_accessor: false do
    "UserMetadatum"
  end
  mattr_accessor :user_metadata_foreign_key, instance_accessor: false do
    :user_id
  end
  mattr_accessor :user_metadata_relation, instance_accessor: false do
    :user_metadata
  end
  mattr_accessor :user_metadata_key, instance_accessor: false do
    :key
  end
  mattr_accessor :user_metadata_value, instance_accessor: false do
    :value
  end
  mattr_accessor :user_token_attribute, instance_accessor: false do
    :token
  end
  mattr_accessor :user_activity_logs_enabled, instance_accessor: false do
    false
  end
  mattr_accessor :user_activity_logs_relation, instance_accessor: false do
    :activity_logs
  end
  mattr_accessor :user_activity_logs_helper_method, instance_accessor: false do
    :create_activity_log
  end
  mattr_accessor :user_activity_logs_survey_type, instance_accessor: false do
    :survey_logs
  end
  mattr_accessor :layout, instance_accessor: false do
    "surveys313/surveys"
  end
  # mattr_accessor :devise_authentification_method, instance_accessor: false do
  #   :admin_authenticate
  # end
  mattr_accessor :devise_current_resource_method, instance_accessor: false do
    :current_admin
  end
  
end
