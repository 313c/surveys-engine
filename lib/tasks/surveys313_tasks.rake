# desc "Explaining what the task does"
namespace :surveys313 do
  desc "Adding testing data to db"
  task :testing_data do
    FactoryGirl.create(:survey, :with_questions)
  end
end
