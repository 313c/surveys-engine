class Survey313Generator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)
    
  def install
    rake "surveys313:install:migrations"
    rake "db:migrate"
    
  end
end
